package com.portfolio.androidapiexample.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.portfolio.androidapiexample.data.User
import com.portfolio.androidapiexample.data.UserRepository

class UserViewModel: ViewModel() {
    private val mUserId: MutableLiveData<String> = MutableLiveData()

    // This method is pretty similar like RxJava method
    val user: LiveData<User> = Transformations.switchMap(mUserId) { userId ->
        UserRepository.getUser(userId)
    }

    fun setUserId(userId: String) {
        if (mUserId.value == userId) {
            return
        }
        mUserId.value = userId
    }

    fun cancelJobs() {
        UserRepository.cancelJob()
    }
}