package com.portfolio.androidapiexample.api

import com.portfolio.androidapiexample.data.User
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("placeholder/{foo}")
    suspend fun getUser(
        @Path("foo") foo: String
    ): User
}