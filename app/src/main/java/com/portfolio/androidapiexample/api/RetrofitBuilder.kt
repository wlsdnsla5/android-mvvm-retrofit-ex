package com.portfolio.androidapiexample.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
    const val BASE_URL = "https://api_web_address"
    val retrofitBuilder: Retrofit.Builder by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
                // since the data will be json
            .addConverterFactory(GsonConverterFactory.create())
    }
    val apiService: ApiService by lazy { retrofitBuilder.build().create(ApiService::class.java) }
}